const express = require('express');

const { get_forecast } = require('./server/forecast');


const PORT = process.env.PORT || 8080;

const app = express();


app.get('/api/v0/forecast', (request, response) => {
  // Set the response type.
  response.type('json');

  const { latitude, longitude } = request.query;

  // Return an HTTP 400 Bad Request response if `latitude' and `longitude' are
  // missing in the query parameters.
  if (!(latitude || longitude)) {
    response.status(400).end();
    return;
  }

  get_forecast(latitude, longitude).then(
    (data) => {
      response.json(data);
    }
  ).catch((error) => {
    // Return an HTTP 500 Internal Server Error response if we can't reach the
    // forecast's endpoint.
    console.log(error);
    response.status(500).end();
  });
});


app.use('/', express.static('build', { fallthrough: true }));


app.listen(PORT);
