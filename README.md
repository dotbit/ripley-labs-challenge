# Desafío Ripley Labs

Desafío consiste en crear un plataforma para ver el clima en distintos lugares
del mundo al seleccionar distintos puntos en un mapa.  La entrega consiste de un
repositorio que contiene el código *frontend* y *backend*.

La aplicacion se puede utilizar en la siguiente url:

[https://radiant-escarpment-13414.herokuapp.com/](https://radiant-escarpment-13414.herokuapp.com/)

El despliegue de la aplicación se realizo a través de
[Heroku](https://www.heroku.com/). Y consiste de un *dyno* junto con un *addon*
de [Redis](https://elements.heroku.com/addons/heroku-redis). Para no exponer
las llaves secretas que se utilizan en la aplicación, se hace uso de
*environment variables* para integrarlas de manera indirecta al código.

Tanto en el *backend* como en el *frontend* se integran por medio de
`process.env`. Sin embargo, en **ReactJS** las variables tienen el prefijo
`REACT_APP_` y son incluidas en el momento de compilación. (Funcionalidad de
**Create React App**.)

Para hacer despliegue de la aplicación en **Heroku** se configurarón las
variables por medio de *configs-vars*. Las que se utilizán al momento de
compilación cuando se ejecuta un *push* del repositorio.


## Ejecutar localmente

Es necesario definir las siguiente *environment variables*:

- `PORT`: Puerto donde esperara solicitudes el servidor. (8080 por defecto.)
- `DARK_SKY_SECRET_KEY`: Llave secreta para realizar solicitudes a la API de **Dark Sky**.
- `REDIS_URL`: *URL* o *host* donde se encuentra **Redis**. (**Heroku**
  automaticamente configura esta variable con el valor correcto para su *addon*.)
- `REACT_APP_GOOGLE_SECRET_KEY`: Llave secreta para utilizar **Google Maps**.
- `REACT_APP_API_ENDPOINT_URL`: *URL* donde se hacen las solicitudes al
  *backend* por si se monta en algún otro servidor. (Asume por defecto
  `/api/v0/forecast`.)


Luego es necesario ejecutar **Redis**, compilar y correr el servidor:

```sh
docker run --name redis-test -p 6379:6379 -d redis:alpine
npm run build
npm run start
```

## Backend

El *backend* esta desarrollado en **NodeJS** utilizando las siguientes
*frameworks* y librerías de **Javascript**:

1. [ExpressJS](https://expressjs.com/),
2. [Request](https://github.com/request/request), y
3. [Redis](https://redis.js.org/).

El punto principal del código del servidor es el archivo `app.js`. El resto del
código se encuentra en la carpeta `server` dentro del proyecto.


## Frontend

El *frontend* esta desarrollado en **ReactJS** y se utilizaron las siguientes
librerías:

1. [ReactJS](https://reactjs.org/),
2. [axios](https://github.com/axios/axios),
3. [Bootstrap](https://getbootstrap.com/),
4. [react-bootstrap](https://react-bootstrap.github.io/), y
5. [google-maps-react](https://www.npmjs.com/package/google-maps-react).

El código del *frontend* esta distribuido en la carpeta `src`; principalmente
`src/js/components`.
