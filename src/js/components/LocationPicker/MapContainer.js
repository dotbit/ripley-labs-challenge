import React from 'react';

import { Map, GoogleApiWrapper } from 'google-maps-react';


const GOOGLE_SECRET_KEY = process.env.REACT_APP_GOOGLE_SECRET_KEY;


const MapContainer = ({google, className = 'map-container', ...props}) => {
  return (
    <div className={className}>
      <Map google={google} {...props} />
    </div>
  );
}


export default GoogleApiWrapper({
  apiKey: GOOGLE_SECRET_KEY,
})(MapContainer);
