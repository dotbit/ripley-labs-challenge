import React, { Component } from 'react';

import MapContainer from './MapContainer';


class LocationPicker extends Component {
  static defaultProps = {
    onLocationSelected: () => {},
  };

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(props, map, click_event) {
    console.log(click_event.pixel.x);
    const result = {
      latitude: click_event.latLng.lat(),
      longitude: click_event.latLng.lng(),
      client_x: click_event.qa.x,
      client_y: click_event.qa.y,
    };

    this.props.onLocationSelected(result);
  }

  render() {
    const { class_name } = this.props;

    return (
      <div className={class_name}>
        <MapContainer onClick={this.onClick} />
      </div>
    );
  }
}


export default LocationPicker;
