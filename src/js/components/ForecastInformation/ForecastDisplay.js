import React, { Component } from 'react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';


class ForecastInformation extends Component {
  render() {
    return (
      <Modal
        {...this.props}
        size="lg"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Forecast information
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul className="list-group">
            <li className="list-group-item"><strong>Latitude:</strong> {this.props.latitude}</li>
            <li className="list-group-item"><strong>Longitude:</strong> {this.props.longitude}</li>
            <li className="list-group-item"><strong>Temperature:</strong> {this.props.temperature} &#8451;</li>
            <li className="list-group-item">
              <div>
                <p className="lead">{this.props.summary}</p>
              </div>
            </li>
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}


export default ForecastInformation;
