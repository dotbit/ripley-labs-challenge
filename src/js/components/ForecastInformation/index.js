import React, { Component } from 'react';

import ForecastDisplay from './ForecastDisplay';
import LocationPicker from '../LocationPicker';
import get_forecast from '../../utils/api';


class ForecastInformation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latitude: '',
      longitude: '',
      temperature: '',
      summary: '',
      show_modal: false,
    }

    this.onLocationSelected = this.onLocationSelected.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  onLocationSelected(l) {
    get_forecast(l.latitude, l.longitude)
      .then((data) => {
        this.setState({
          latitude: data.latitude,
          longitude: data.longitude,
          temperature: data.currently.apparentTemperature,
          summary: data.currently.summary,
          icon: data.currently.icon,
          show_modal: true,
        });
      })
      .catch((error) => console.log(error));
  }

  closeModal() {
    this.setState({
      show_modal: false,
    });
  }

  render() {
    const { latitude, longitude, temperature, summary, icon, show_modal} = this.state;

    const hideModal = () => this.setState({ show_modal: false });

    return (
      <div className="row">
        <div className="col px-0 min-vh-100">
          <LocationPicker onLocationSelected={this.onLocationSelected} />
        </div>
        <ForecastDisplay
          latitude={latitude}
          longitude={longitude}
          temperature={temperature}
          summary={summary}
          icon={icon}
          show={show_modal}
          onHide={hideModal}
        />
      </div>
    );
  }
}


export default ForecastInformation;
