import React from 'react';

import clear_day from '../../../icons/clear-day.png';
import clear_night from '../../../icons/clear-night.png';
import rain from '../../../icons/rain.png';
import snow from '../../../icons/snow.png';
import sleet from '../../../icons/sleet.png';
import wind from '../../../icons/wind.png';
import fog from '../../../icons/fog.png';
import cloudy from '../../../icons/cloudy.png';
import partly_cloudy_day from '../../../icons/partly-cloudy-day.png';
import partly_cloudy_night from '../../../icons/partly-cloudy-night.png';


class ForecastIcon extends React {
  get_icon(icon) {
    switch (icon) {
      case 'clear-day':
        return clear_day;
      case 'clear-night':
        return clear_night;
      case 'rain':
        return rain;
      case 'snow':
        return snow;
      case 'sleet':
        return sleet;
      case 'wind':
        return wind;
      case 'fog':
        return fog;
      case 'cloudy':
        return cloudy;
      case 'partly-cloudy-day':
        return partly_cloudy_day;
      case 'partly-cloudy-night':
        return partly_cloudy_night;
      default:
        return ''
    }
  }

  render() {
    const { icon } = this.props;

    return (
      <img src={this.get_icon(icon)} />
    );
  }
}


export default ForecastIcon;
