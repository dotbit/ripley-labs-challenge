import axios from 'axios';


// Change if server is on another URL.
const API_ENDPOINT_URL = process.env.REACT_APP_API_ENDPOINT_URL || '/api/v0/forecast';


const get_forecast = async (latitude, longitude) => {
  const request_url = `${API_ENDPOINT_URL}?latitude=${latitude}&longitude=${longitude}`;
  const data = await axios.get(request_url);
  return data.data;
};


export default get_forecast;
