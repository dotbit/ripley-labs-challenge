import React from 'react';
import './App.scss';

import ForecastInformation from './js/components/ForecastInformation';


function App() {
  return (
    <div className="container-fluid vh-100">
      <nav class="navbar fixed-top navbar-light bg-light">
          <h1>Discover the weather</h1>
      </nav>
      <div className="row min-vh-100">
        <div className="col mx-0">
          <ForecastInformation />
        </div>
      </div>
    </div>
  );
}

export default App;
