const { expect } = require('chai');

const { Cache } = require('../server/cache');



describe('Cache', () => {
  // Make sure that the cache client is connected before tests.
  let cache;

  const create_cache = () => {
    return new Promise((resolve, reject) => {
      cache = new Cache('', 'TEST');

      cache._client.on('connect', (error) => {
        if (error) {
          reject(error);
        }
        resolve(cache);
      });
    });
  }

  before(() => create_cache());

  // const cache = await create_cache();

  describe('#set', () => {
    it('Succesfully sets a value in the cache', async () => {
      await cache.set('test', 'value', 2000);
    });
  });

  describe('#get', () => {
    it('Retrieves the correct value from the cache', async () => {
      await cache.set('test', 'value', 2000);

      const value = await cache.get('test');

      expect(value).to.equal('value');
    });
  });

});
