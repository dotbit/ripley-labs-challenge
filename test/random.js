const { expect } = require('chai');

const { random_fail } = require('../server/utils/random');


describe('Random', () => {
  describe('.random_fail', () => {
    it('Returns false with +/- 5% margin rate after 2000 tries.', () => {
      const TRIES = 2000;
      const FAIL_RATE = 0.35;
      const DELTA = 0.05;
      const TOP_MARGIN = FAIL_RATE + DELTA;
      const BOTTOM_MARGIN = FAIL_RATE - DELTA;

      let total = 0;

      for (let i = 0; i < TRIES; i++) {
        total += random_fail(FAIL_RATE) ? 0 : 1;
      }

      const error_percetage = total / TRIES;

      expect(error_percetage).to.be.below(TOP_MARGIN);
      expect(error_percetage).to.be.above(BOTTOM_MARGIN);
    });
  });
});
