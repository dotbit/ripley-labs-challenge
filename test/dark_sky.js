const { expect } = require('chai');

const { dark_sky_get_forecast } = require('../server/utils/dark_sky');
const { dark_sky_get_forecast_faulty } = require('../server/utils/dark_sky');


describe('Dark Sky API', () => {
  describe('.dark_sky_get_forecast', () => {
    xit('Returns an object', async () => {
      const SECRET_KEY = process.env.DARK_SKY_SECRET_KEY;
      const result = await dark_sky_get_forecast(SECRET_KEY, 10, 10);

      expect(result).to.be.an('object');
    });

    it('Fails if `secret_key\' not present', async () => {
      // Ugly workaround.
      try {
        const result = await dark_sky_get_forecast(SECRET_KEY, 10, 10);
        expect(1).to.equal(2);
      } catch (e) {
        expect(1).to.equal(1);
      }
    });
  });

  describe('.dark_sky_get_forecast_faulty', () => {
    // Exclude test to avoid exhausting Dark Sky's API quota.
    xit('Fails with +/- 5% margin rate after 1000 tries.', async () => {
      const TRIES = 1000;
      const FAIL_RATE = 0.1; // Challenge fail rate.
      const DELTA = 0.05;
      const TOP_MARGIN = FAIL_RATE + DELTA;
      const BOTTOM_MARGIN = FAIL_RATE - DELTA;
      const SECRET_KEY = process.env.DARK_SKY_SECRET_KEY;

      let total = 0;

      for (let i = 0; i < TRIES; i++) {
        try {
          await dark_sky_get_forecast_faulty(SECRET_KEY, 10, 10);
        } catch(error) {
          total_errors += 1;
        }
      }

      const error_percetage = total / TRIES;

      expect(error_percetage).to.be.below(TOP_MARGIN);
      expect(error_percetage).to.be.above(BOTTOM_MARGIN);
    });
  });
});
