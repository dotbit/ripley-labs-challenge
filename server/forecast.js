const { dark_sky_get_forecast_faulty } = require('./utils/dark_sky');
const { Cache } = require('./cache');
const { force_function } = require('./utils/force_function');


const REDIS_URL = process.env.REDIS_URL || '';
const SECRET_KEY = process.env.DARK_SKY_SECRET_KEY;


// Forecast cache specialization.
class ForecastCache extends Cache {
  constructor(host, root_key = 'FORECAST') {
    super(host, root_key);
  }

  get(latitude, longitude) {
    const sub_key = `${latitude}:${longitude}`;

    return super.get(sub_key);
  }

  set(latitude, longitude, value, expiration = 60) {
    const sub_key = `${latitude}:${longitude}`;

    return super.set(sub_key, value, expiration);
  }
}


// Global forecast cache.
const CACHE = new ForecastCache(REDIS_URL);


const get_forecast = async (latitude, longitude) => {
  let data;

  // Try to get the data from the cache.
  try {
    const raw_data = await CACHE.get(latitude, longitude);
    data = JSON.parse(raw_data);
  } catch (error) {
    console.log(error);
  }

  // If there is data return it.
  if (data) {
    console.log('FROM CACHE');
    return data;
  }

  // Get data from Dark Sky's API and save it to the cache. Uses
  // `force_function' in order to retry indefinitely until
  // `dark_sky_get_forecast_faulty' succeeds. Return the result independently if
  // the are errors when saving to the cache.
  data = await force_function(
    () => dark_sky_get_forecast_faulty(SECRET_KEY, latitude, longitude)
  );

  try {
    const raw_data = JSON.stringify(data);
    CACHE.set(latitude, longitude, raw_data);
  } catch (error) {
    console.log(error);
  }

  return data;
};


module.exports = {
  get_forecast,
};
