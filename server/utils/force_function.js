// Repeatedly executes a function until it doesn't throw an error and returns
// its result.
const force_function = async (async_function) => {
  let value;
  let succeded = false;

  while (!succeded) {
    try {
      value = await async_function();
      succeded = true;
    } catch (error) {
      console.log(error);
    }
  }

  return value;
};


module.exports = {
  force_function,
}
