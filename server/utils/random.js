const random_fail = (fail_rate) => Math.random() <= fail_rate ? false : true;


module.exports = {
  random_fail,
};
