const request = require('request');

const { random_fail } = require('./random');


// Base URL for Dark Sky's forecast endpoint.
const ENDPOINT_URL = 'https://api.darksky.net/forecast';


const dark_sky_get_forecast = (secret_key, latitude, longitude) => {
  const endpoint = `${ENDPOINT_URL}/${secret_key}/${latitude},${longitude}?units=si`;
  return new Promise((resolve, reject) => {
    let data;

    request.get(endpoint, (error, response, body) => {
      if (error) {
        reject(error);
      } else {
        try {
          data = JSON.parse(body);
        } catch(e) {
          reject(e);
        }

        resolve(data);
      }
    });
  });
};


// Fail rate for faulty function (10%).
const FAIL_RATE = 0.1;

// Same as `dark_sky_get_forecast' but throws an error with a 10% chance.
const dark_sky_get_forecast_faulty = (secret_key, latitude, longitude) => {
  return new Promise((resolve, reject) => {
    if (random_fail(FAIL_RATE)) {
      resolve(dark_sky_get_forecast(secret_key, latitude, longitude));
    } else {
      reject(new Error('Random error.'));
    }
  });
};


module.exports = {
  dark_sky_get_forecast,
  dark_sky_get_forecast_faulty,
};
