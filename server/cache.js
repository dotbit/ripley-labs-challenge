const redis = require('redis');


const REDIS_URL = process.env.REDIS_URL || '';


class Cache {
  constructor(host, key_root) {
    this._client = redis.createClient(host);
    this._key_root = key_root;
  }

  get(key) {
    const _key = `${this._key_root}:${key}`;

    return new Promise((resolve, reject) => {
      this._client.get(_key, (error, reply) => {
        if (error) {
          reject(error);
        } else if (reply === null) {
          reject(new Error('No value'));
        } else {
          resolve(reply);
        }
      });
    });
  }

  set(key, value, expiration) {
    const _key = `${this._key_root}:${key}`;

    return new Promise((resolve, reject) => {
      this._client.set(_key, value, 'EX', expiration, (error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(null);
        }
      });
    });
  }
}


module.exports = {
  Cache,
};
